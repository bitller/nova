<?php

return [
    'choose_notification_type' => 'Alege tipul notificării',
    'create_new_notification' => 'Crează o notificare nouă',
    'created_at' => 'Data creării',
    'delete' => 'Şterge',
    'edit_notification_title' => 'Editează titlul notificării',
    'hide_all_notifications' => 'Ascunde toate notificările',
    'message' => 'Mesaj',
    'new_notification_title' => 'Noul titlu al notificării',
    'notification_created' => 'Notificarea a fost creată!',
    'notification_deleted' => 'Notificarea a fost ștearsă!',
    'notification_title_updated' => 'Titlul notificării a fost actualizat!',
    'notifications' => 'Notificări',
    'notifications_manager' => 'Manager de notificări',
    'show_all_notifications' => 'Afişează toate notificările',
    'title' => 'Title',
    'type' => 'Tip',
];