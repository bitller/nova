<?php

return [
    'add_products_by_code'      => 'Adaugă produsele după codul lor',
    'add_products_by_code_long' => 'Pentru a adaugă un produs la o factură, trebuie doar să introduceți codul produsului din catalogul avon iar de restul ne ocupăm noi.',
    'fast_access'               => 'Access rapid la toate facturile',
    'fast_access_long'          => 'Vrei să găsești o factură de acum o lună sau un an? Nimic mai simplu. Ai access rapid la toate facturile.',
    'fast_bill_creation'        => 'Creează o nouă factură în câteva secunde',
    'fast_bill_creation_long'   => 'Ai primit o nouă comandă? Durează doar câteva secunde să creezi și să tipărești o nouă factură.',
    'first_ok' => 'Creezi și tipărești facturi pentru comenzile clienților tăi.',
    'principal_title' => 'Manager pentru comenzile Avon',
    'print_bills'               => 'Imprimează facturile simplu și rapid',
    'print_bills_long'          => 'Ai creat o nouă comandă? Ai adăugat toate produsele și totul este gata? Atunci imprimează factura pentru a o da clientului.',
    'search_by_code'            => 'Caută un produs după cod',
    'search_by_code_long'       => 'Puteți caută rapid un produs în bază noastră de date după codul său din catalogul Avon.',
    'second_ok' => 'Facturi cu aspect elegant, ce donota profesionalism și respect, două valori care contează tot mai mult în ochii clienților.',
    'start'                     => 'Începe să folosești Nova',
    'statistics'                => 'Statistici despre vânzări',
    'statistics_long'           => 'Aveți la dispoziție statistici despre vânzările efectuate, cele mai vândute produse, campania cu cele mai mari vânzări și multe altele.',
    'third_ok' => 'Ai acces de oriunde la comenzile clienților tăi, statistici despre vânzările efectuare și performanțele tale ca reprezentant.',
    'welcome'                   => 'Bun venit',
    'what_is_nova'              => 'Nova este o aplicație de facturare și management al facturilor dedicată reprezentanților Avon',
    'why'                       => 'Nu ești convins încă?',
];