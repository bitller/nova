<?php

return [
    'bills_that_contain_product' => 'Facturile neplătite în care apare acest produs',
    'client_name' => 'Numele clientului',
    'code_already_used' => 'Codul introdus aparține altui produs.',
    'code_updated' => 'Codul produsului a fost actualizat! Pagina va fi reîncărcată.',
    'edit_code' => 'Editează codul',
    'edit_code_description' => 'Editează codul produsului',
    'edit_error' => 'Acest produs nu există sau nu poate fi editat. Doar produsele adăugate de tine pot fi editate.',
    'edit_name' => 'Editează numele',
    'edit_name_description' => 'Editează numele produsului',
    'delete_product' => 'Şterge produsul',
    'name_updated' => 'Numele produsului a fost schimbat!',
    'number_of_sold_pieces' => 'Numărul de bucăţi vândute',
    'paid_bills_that_contain_product' => 'Facturile plătite în care apare acest produs',
    'product_code_input_required' => 'Introduceți noul code al produsului!',
    'product_deleted' => 'Produsul a fost şters!',
    'product_name_input_required' => 'Introduceți noul nume al produsului!',
    'product_statistics' => 'Statisticile produsului',
    'total_price' => 'Preţ total'
];