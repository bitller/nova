<?php

return [
    'add' => 'Adaugă produs',
    'avon_products' => 'Produsele din catalog',
    'code' => 'Codul produsului',
    'edit_name' => 'Editează numele',
    'my_products' => 'Produsele mele',
    'name' => 'Numele produsului',
    'page_help' => 'Aici găsești toate produsele din catalogul Avon, sortate în mod crescător după codul unic format din 5 cifre.',
    'product_code_tooltip' => 'Codul produsului din catalog',
    'product_name_edited' => 'Numele produsului a fost actualizat!',
    'product_name_tooltip' => 'Numele produsului din catalog',
    'products_list' => 'Lista cu produse',
    'tooltip' => 'Numărul de produse Avon',
];