<?php

return [
    'choose_notification_type' => 'Choose notification type',
    'create_new_notification' => 'Create new notification',
    'created_at' => 'Created at',
    'delete' => 'Delete',
    'edit_notification_title' => 'Edit notification title',
    'hide_all_notifications' => 'Hide all notifications',
    'message' => 'Message',
    'new_notification_title' => 'New notification title',
    'notification_created' => 'Notification created!',
    'notification_deleted' => 'Notification deleted!',
    'notification_title_updated' => 'Notification title updated!',
    'notifications' => 'Notifications',
    'notifications_manager' => 'Notifications manager',
    'show_all_notifications' => 'Show all notifications',
    'title' => 'Title',
    'type' => 'Type',
];