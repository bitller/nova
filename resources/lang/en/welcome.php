<?php

return [
    'add_products_by_code'      => 'Add products by their code',
    'add_products_by_code_long' => 'To add a new product to a bill just insert their code from the Avon catalogue and we will take care of the rest.',
    'fast_access'               => 'Fast access to your bills',
    'fast_access_long'          => 'Do you want to find a bill from one month or one year ago? Simple! You have fast access to all your bills.',
    'fast_bill_creation'        => 'Create new bill in few seconds',
    'fast_bill_creation_long'   => 'Did you get a new order? Takes only few seconds to create a new bill.',
    'first_ok' => 'Create and print one bill for each client order',
    'principal_title' => 'Avon orders manager',
    'print_bills'               => 'Print your bills fast and simple',
    'print_bills_long'          => 'Did you get a new order? Did you added all products? Now it\'s time to print the bill.',
    'search_by_code'            => 'Search a product by their code',
    'search_by_code_long'       => 'You can search through all Avon products by their code.',
    'second_ok' => 'Bills with elegant aspect, which denotes professionalism and respect, two values that matters more and more for your clients',
    'start'                     => 'Start using Nova',
    'statistics'                => 'Statistics about sellings',
    'statistics_long'           => 'You have statistics about your sellings, most selled products, campaign with biggest sellings and others.',
    'third_ok' => 'You have access to all your orders and statistics about sells from anywhere',
    'welcome'                   => 'Welcome',
    'what_is_nova'              => 'Nova is a billing and products management application for Avon representatives',
    'why'                       => 'Why use Nova?'
];